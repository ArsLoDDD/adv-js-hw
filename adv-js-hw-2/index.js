const books = [
	{
		author: 'Люсі Фолі',
		name: 'Список запрошених',
		price: 70,
	},
	{
		author: 'Сюзанна Кларк',
		name: 'Джонатан Стрейндж і м-р Норрелл',
	},
	{
		name: 'Дизайн. Книга для недизайнерів.',
		price: 70,
	},
	{
		author: 'Алан Мур',
		name: 'Неономікон',
		price: 70,
	},
	{
		author: 'Террі Пратчетт',
		name: 'Рухомі картинки',
		price: 40,
	},
	{
		author: 'Анґус Гайленд',
		name: 'Коти в мистецтві',
	},
]

const container = document.querySelector('#root')
const list = document.createElement('ul')
container.append(list)

books.forEach(book => {
	try {
		if (Object.keys(book).length < 3) {
			if (book.author === undefined) {
				throw new TypeError('Вiдсутнiй автор')
			} else if (book.name === undefined) {
				throw new TypeError('Вiдсутня назва')
			} else if (book.price === undefined) {
				throw new TypeError('Вiдсутня цiна')
			}
		}
		const listItem = document.createElement('li')
		listItem.textContent = `${book.author} - ${book.name} - ${book.price} грн`
		list.append(listItem)
	} catch (error) {
		console.log(error)
	}
})
