https://ajax.test-danit.com/api-pages/cards.html

При кліку на кнопку Login має відкриватися модальне вікно, в якому має бути форма авторизації.
По кліку на Confirm модального вікна атворизовуватись на сервері і зберігати токен в localStorage. У авторизованого
юзера кнопка Login має змінюватися на Add Element.

По кліку на кнопку 'Add Element' має відкриватися модальне вікно, в якому має бути форма з селектом
вибору типу елемента. По кліку на бекграунд та кнопку закриття модального вікна, вікно закривається.

При виборі елемента в формі мають з'являтися відповідні поля для введення даних.

По кліку на кнопку 'Confirm' відповідний елемент має додаватись в кінець списку елементів. (Зберігати зміни на сервері)

Реалзіувати можливість видалення елементів зі списку. (Зберігати зміни на сервері)

<hr>

Структура модального вікна:

```html
<div class="modal">
	<div class="modal__background"></div>

	<div class="modal__main-container">
		<button class="modal__close"></button>

		<div class="modal__content-wrapper"></div>

		<div class="modal__button-wrapper">
			<button class="modal__confirm-btn">Confirm</button>
		</div>
	</div>
</div>
```

Структура загальної форми авторизації:

```html
<form class="login-form">
	<h3>Login</h3>
	<input type="text" name="email" placeholder="Your Email" id="email" />
	<input
		type="text"
		name="password"
		placeholder="Your Password"
		id="password"
	/>
</form>
```

Структура загальної форми з селектом:

```html
<form class="order-form">
	<h3>Choose the element type</h3>
	<select>
		<option value="" selected="">Choose the element type</option>x
		<option value="button">Button</option>
		<option value="input">Input</option>
		<option value="image">Image</option>
		<option value="paragraph">Paragraph</option>
	</select>
</form>
```

Структура форми для параграфа:

```html
<form class="order-form">
	<h3>Choose the element type</h3>
	<select>
		<option value="" selected="">Choose the element type</option>
		<option value="button">Button</option>
		<option value="input">Input</option>
		<option value="image">Image</option>
		<option value="paragraph">Paragraph</option>
	</select>

	<div>
		<h3>Paragraph</h3>
		<form class="elem__form">
			<input type="text" placeholder="ID" name="id" id="id" />
			<input type="text" placeholder="Classes" name="classes" id="classes" />
			<input type="text" placeholder="Text" name="text" id="text" />
		</form>
	</div>
</form>
```

Є два можливих типа елементів:

## Параграф

У параграфа має бути:

- Текст
- id
- Класи

## Зображення

У зображення має бути:

- id
- Класи
- src
- alt
- width
- height
