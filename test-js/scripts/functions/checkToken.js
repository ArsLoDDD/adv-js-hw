import { loginBtn, logOut, addBtn } from '../constants/btns.js'
export default function checkToken() {
	const token = localStorage.getItem('token')
	if (token) {
		loginBtn.style.display = 'none'
		logOut.style.display = 'block'
		addBtn.style.display = 'block'
		return true
	} else {
		loginBtn.style.display = 'block'
		logOut.style.display = 'none'
		addBtn.style.display = 'none'
		return false
	}
}
