import checkToken from '../functions/checkToken.js'
import postElement from '../API/postElement.js'

export default class ModalWindow {
	constructor(elem, confirmFunction) {
		this.elem = elem
		this.confirmFunction = confirmFunction
		this.modal = document.createElement('div')
		this.modalBackground = document.createElement('div')
		this.modalMainContainer = document.createElement('div')
		this.modalCloseBtn = document.createElement('button')
		this.modalContentWrapper = document.createElement('div')
		this.modalButtonWrapper = document.createElement('div')
		this.modalConfirmBtn = document.createElement('button')
	}
	createElements() {
		this.modal.classList.add('modal')
		this.modalBackground.classList.add('modal__background')
		this.modalMainContainer.classList.add('modal__main-container')
		this.modalCloseBtn.classList.add('modal__close')
		this.modalContentWrapper.classList.add('modal__content-wrapper')
		this.modalButtonWrapper.classList.add('modal__button-wrapper')
		this.modalConfirmBtn.classList.add('modal__confirm-btn')

		this.modalConfirmBtn.innerText = 'Confirm'

		this.modalMainContainer.append(
			this.modalCloseBtn,
			this.modalContentWrapper,
			this.modalButtonWrapper
		)
		this.modalContentWrapper.append(this.elem)
		this.modalButtonWrapper.append(this.modalConfirmBtn)
		this.modal.append(this.modalBackground, this.modalMainContainer)
	}
	addEvents() {
		this.modalCloseBtn.addEventListener('click', () => {
			this.close()
		})
		this.modalBackground.addEventListener('click', () => {
			this.close()
		})
		this.modalConfirmBtn.addEventListener('click', () => {
			this.confirmHandler()
		})
	}
	async confirmHandler() {
		if (localStorage.getItem('token') === null) {
			const status = await this.confirmFunction({
				email: this.elem.children[1].value,
				password: this.elem.children[2].value,
			})
			console.log(status)
			if (status === 200) {
				this.close()
				checkToken()
			}
		} else {
			const obj = {}
			const arr = [...this.elem.children[2].children]
			arr.forEach(element => {
				if (element.getAttribute('name') === 'id') {
					obj['elemId'] = element.value
					return
				}
				obj[element.getAttribute('name')] = element.value
				obj['tag'] = element.getAttribute('data-my-tag')
			})
			const response = await postElement(obj)
			console.log(response)
			if (response.status === 200) {
				let newElem
				if (obj.tag === 'img') {
					newElem = `<${obj.tag} id="${obj.elemId}" class="${obj.classes}" src="${obj.src}" width="${obj.width}px" height="${obj.height}px">`
				} else {
					newElem = `<${obj.tag} id="${obj.elemId}" class="${obj.classes}">${obj.text}</${obj.tag}>`
				}
				document.body.insertAdjacentHTML('beforeend', newElem)
				this.close()
			}
		}
	}
	close() {
		this.modal.remove()
	}
	render() {
		this.createElements()
		this.addEvents()

		document.body.append(this.modal)
	}
}

{
	/* <div class="modal">
  <div class="modal__background"></div>

  <div class="modal__main-container">
    <button class="modal__close"></button>

    <div class="modal__content-wrapper"></div>

    <div class="modal__button-wrapper">
      <button class="modal__confirm-btn">Confirm</button>
    </div>

  </div>
</div> */
}
