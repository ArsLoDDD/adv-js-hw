export default class Form {
	constructor() {
		this.formContainer = document.createElement('form')
		this.formTitle = document.createElement('h3')
		this.formEmailInput = document.createElement('input')
		this.formPasswordInput = document.createElement('input')
	}
	createElements() {
		this.formContainer.classList.add('login-form')
		this.formTitle.innerText = 'Login'
		this.formEmailInput.setAttribute('type', 'text')
		this.formEmailInput.setAttribute('placeholder', 'Your Email')
		this.formEmailInput.setAttribute('name', 'email')
		this.formEmailInput.setAttribute('id', 'email')
		this.formPasswordInput.setAttribute('type', 'password')
		this.formPasswordInput.setAttribute('placeholder', 'Your Password')
		this.formPasswordInput.setAttribute('name', 'password')
		this.formPasswordInput.setAttribute('id', 'password')

		this.formContainer.append(
			this.formTitle,
			this.formEmailInput,
			this.formPasswordInput
		)
	}
	render() {
		this.createElements()
		return this.formContainer
	}
}
