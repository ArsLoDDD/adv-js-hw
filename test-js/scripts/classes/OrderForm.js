export default class OrderForm {
	constructor() {
		this.formContainer = document.createElement('form')
		this.formTitle = document.createElement('h3')
		this.select = document.createElement('select')
		this.newFormContainer = document.createElement('div')
	}
	createElements() {
		this.formContainer.classList.add('order-form')
		this.formTitle.innerText = 'Choose the element type'

		const option1 = document.createElement('option')
		option1.value = ''
		option1.selected = true
		option1.textContent = 'Choose the element type'

		const option2 = document.createElement('option')
		option2.value = 'button'
		option2.textContent = 'Button'

		const option3 = document.createElement('option')
		option3.value = 'input'
		option3.textContent = 'Input'

		const option4 = document.createElement('option')
		option4.value = 'img'
		option4.textContent = 'Image'

		const option5 = document.createElement('option')
		option5.value = 'p'
		option5.textContent = 'Paragraph'

		this.select.appendChild(option1)
		this.select.appendChild(option2)
		this.select.appendChild(option3)
		this.select.appendChild(option4)
		this.select.appendChild(option5)

		this.formContainer.appendChild(this.formTitle)
		this.formContainer.appendChild(this.select)
		this.formContainer.appendChild(this.newFormContainer)

		this.select.addEventListener('change', e => {
			this.newFormContainer.innerHTML = ''
			const form = this.createForm(e.target)
			this.newFormContainer.innerHTML = form.form
			this.formTitle.innerText = `Create ${form.type}`
		})
	}
	createForm(form) {
		console.log(form.value)
		if (
			form.value === 'p' ||
			form.value === 'input' ||
			form.value === 'button'
		) {
			const newForm = `<form class="elem__form">
			<input type="text" data-my-tag=${form.value} placeholder="ID" name="id" id="id" />
			<input type="text" data-my-tag=${form.value} placeholder="Classes" name="classes" id="classes" />
			<input type="text" data-my-tag=${form.value} placeholder="Text" name="text" id="text" />
		</form>`
			return { form: newForm, type: form.options[form.selectedIndex].innerText }
		}
		if (form.value === 'img') {
			const newForm = `<form class="elem__form">
			<input type="text" data-my-tag=${form.value}  placeholder="ID" name="id" id="id" />
			<input type="text" data-my-tag=${form.value}  placeholder="Classes" name="classes" id="classes" />
			<input type="text" data-my-tag=${form.value}  placeholder="SRC" name="src" id="src" />
			<input type="text" data-my-tag=${form.value}  placeholder="ALT" name="alt" id="alt" />
			<input type="text" data-my-tag=${form.value}  placeholder="Width" name="width" id="width" />
			<input type="text" data-my-tag=${form.value}  placeholder="Height" name="height" id="height" />
		</form>`
			return { form: newForm, type: form.options[form.selectedIndex].innerText }
		}
		return { form: '', type: 'Choose the element type' }
	}
	render() {
		this.createElements()
		return this.formContainer
	}
}
