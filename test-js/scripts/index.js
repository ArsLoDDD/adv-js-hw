import Form from './classes/Form.js'
import OrderForm from './classes/OrderForm.js'
import ModalWindow from './classes/ModalWindow.js'
import loginFunction from './API/loginFunction.js'
import checkToken from './functions/checkToken.js'
import { loginBtn, logOut, addBtn } from './constants/btns.js'
import loadContent from './API/loadContent.js'
import deleteFun from './API/deleteFun.js'
const check = checkToken()
if (check) {
	;(async () => {
		console.log(await loadContent())
	})()
}
// ;(async () => {
// 	const { data } = await loadContent()
// 	console.log(data)
// 	data.forEach(element => {
// 		deleteFun(element.id)
// 		console.log('1')
// 	})
// })()

loginBtn.addEventListener('click', () => {
	new ModalWindow(new Form().render(), loginFunction).render()
})
logOut.addEventListener('click', () => {
	localStorage.removeItem('token')
	document.location.reload()
	checkToken()
})
addBtn.addEventListener('click', () => {
	new ModalWindow(new OrderForm().render()).render()
})
