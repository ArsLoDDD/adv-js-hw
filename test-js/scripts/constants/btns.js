const loginBtn = document.querySelector('.js-login-btn')
const logOut = document.querySelector('.js-logout-btn')
const addBtn = document.querySelector('.js-create-elem-btn')
export { loginBtn, logOut, addBtn }
