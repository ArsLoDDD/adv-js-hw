import instance from './instance.js'

export default function postElement(element) {
	return instance.post('/', element).then(res => res)
}
