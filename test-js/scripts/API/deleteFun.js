import instance from './instance.js'

export default function deleteElement(id) {
	return instance.delete(`/${id}`).then(res => console.log(res))
}
