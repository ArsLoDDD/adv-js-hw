import instance from './instance.js'
export default function loadContent() {
	return instance.get(`/`).then(res => {
		let newElem

		res.data.forEach(element => {
			if (element.tag === 'img') {
				
				newElem = `<${element.tag} id="${element.elemId}" class="${element.classes}" src="${element.src}" width="${element.width}px" height="${element.height}px">`
			} else {
				newElem = `<${element.tag} id="${element.elemId}" class="${element.classes}">${element.text}</${element.tag}>`
			}
			document.body.insertAdjacentHTML('beforeend', newElem)
		})
		return res.data
	})
}
