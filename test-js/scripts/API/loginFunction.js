import instance from './instance.js'

export default function loginFunction(obj) {
	return instance
		.post('/login', obj, {
			headers: {
				'Content-Type': 'application/json',
			},
		})
		.then(response => {
			localStorage.setItem('token', response.data)
			document.location.reload()
			return response.status
		})
		.catch(error => {
			console.log(error)
			return error.response.status
		})
}
