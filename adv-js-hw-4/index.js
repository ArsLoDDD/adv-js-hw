const body = document.querySelector('body')
axios
	.get('https://ajax.test-danit.com/api/swapi/films')
	.then(({ data }) => {
		console.log(data)
		const sortData = data.sort((a, b) => a.episodeId - b.episodeId)
		const loadCard = `<div class="movie-card"><div class="spinner"></div></div>`
		body.insertAdjacentHTML('beforeend', loadCard)
		body.innerHTML = ''
		sortData.forEach(({ episodeId, name, openingCrawl }) => {
			const filmCard = `<div class="movie-card">
		<h2>Episode <span class="episode-number">${episodeId}</span>: <span class="movie-title">${name}</span></h2>
		<p class="movie-summary">${openingCrawl}</p>
		<h3>Characters:
		<div class="characters-box"><div class="spinner"></div>
		<ul class="character-list"></ul></div>
		
		</h3>
	</div>`
			body.insertAdjacentHTML('beforeend', filmCard)
		})
		return sortData
	})
	.then(data => {
		const charactersBox = document.querySelectorAll('.character-list')
		const spinners = document.querySelectorAll('.spinner')
		const characters = data.map(({ characters }) => {
			return Promise.all(
				characters.map(character => {
					return axios
						.get(character)
						.then(({ data: { name, height, mass, gender } }) => {
							return {
								name,
								height,
								mass,
								gender,
							}
						})
				})
			)
		})

		Promise.all(characters)
			.then(data => {
				console.log(data)
				charactersBox.forEach((box, index) => {
					console.log(data[index])
					data[index].forEach(({ name, height, mass, gender }) => {
						box.insertAdjacentHTML(
							'beforeend',
							`<li class="character">${name} | Gender: ${
								gender ? gender : 'Havent gender'
							} | Height: ${height} | Weight: ${
								mass ? mass : 'Dont have data'
							}</li>`
						)
					})
					spinners[index].remove()
				})
			})
			.catch(err => console.log(err))
	})

/**	<ul class="character-list">
			<li class="character">Character 1</li>
			<li class="character">Character 2</li>
			<li class="character">Character 3</li>
		</ul> 
		data.forEach(({ characters }) => {
				const ul = charactersBox.insertAdjacentHTML('beforeend', ul)
			})
		${characters.forEach(character => {
				axios.get(character).then(({ data }) => {
					console.log(data.name)
					return `<li class="character">${data.name}</li>`
				})
			})}
		
		*/
