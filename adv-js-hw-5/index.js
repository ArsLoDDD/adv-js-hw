const user = {
	id: 13,
	name: 'Arseniy Ablogin',
}

class Card {
	constructor(
		userId,
		title,
		body,
		likes = 0,
		time = 'Just Now',
		change = false
	) {
		this.userId = userId
		this.title = title
		this.body = body
		this.likes = likes
		this.time = time
		this.change = change
	}

	createHTM(user, postId) {
		return `<div class="post-card" data-user-id="${postId}">
      <div class="post-card-avatar">
        <img src="https://xsgames.co/randomusers/assets/avatars/pixel/${
					this.userId
				}.jpg" alt="Avatar">
      </div>
      <div class="post-card-content">
        <div class="post-card-header">
          <span class="post-card-user">${user.name}</span>
          <span class="post-card-time">${this.time}</span>
        </div>
        <div class="post-card-title">
          ${this.title}
        </div>
        <div class="post-card-text">
          ${this.body}
        </div>
				<div> 
				<div class="likes">
				<svg class="svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" /></svg>
				<span class="likes-count">${this.likes}</span>
				</div>
				</div>
        <button class="delete-button">Delete</button>
				${this.change ? '<button class="edit-button">Edit</button>' : ''}
      </div>
    </div>`
	}
}

const container = document.querySelector('.card__container')

function compareFunction() {
	return Math.random() - 0.5
}
function getRandomNumber() {
	return Math.floor(Math.random() * 10000)
}
const usersPromise = axios.get('https://ajax.test-danit.com/api/json/users')
const postsPromise = axios.get('https://ajax.test-danit.com/api/json/posts')

Promise.allSettled([usersPromise, postsPromise])
	.then(([usersResult, postsResult]) => {
		if (
			usersResult.status === 'fulfilled' &&
			postsResult.status === 'fulfilled'
		) {
			const users = usersResult.value.data
			const posts = postsResult.value.data

			const sortPosts = posts.sort(compareFunction)
			sortPosts.forEach(({ id, userId, title, body }) => {
				const user = users.find(({ id }) => id === userId)
				const card = new Card(
					userId,
					title,
					body,
					getRandomNumber(),
					'2 hours ago'
				)
				const cardHTML = card.createHTM(user, id)
				container.insertAdjacentHTML('beforeend', cardHTML)
			})
		} else {
			throw new Error('Error while receiving users or posts')
		}
	})
	.then(() => {
		const deleteButtons = document.querySelectorAll('.delete-button')
		const likes = document.querySelectorAll('.svg')
		likes.forEach(like => {
			like.addEventListener('click', event => {
				likeFunction(event)
			})
		})
		deleteButtons.forEach(button => {
			button.addEventListener('click', event => {
				deleteFunction(event)
			})
		})
	})
	.catch(error => {
		console.log(error)
	})

const likeFunction = event => {
	const countElement = event.target.parentNode.nextSibling.nextSibling
	const count = Number(countElement.textContent)
	console.log(countElement)
	if (event.target.classList.contains('active')) {
		event.target.classList.remove('active')
		countElement.textContent = count - 1
	} else {
		event.target.classList.add('active')
		countElement.textContent = count + 1
	}
}
const deleteFunction = event => {
	axios
		.delete(
			`https://ajax.test-danit.com/api/json/posts/${event.target.parentNode.parentNode.getAttribute(
				'data-user-id'
			)}`
		)
		.then(({ status }) => {
			if (status === 200) {
				event.target.parentNode.parentNode.remove()
			}
		})
		.catch(error => {
			console.log(error)
		})
}

const editFunction = event => {
	modalFunction(
		true,
		event.target.parentNode.parentNode.getAttribute('data-user-id')
	)
	const sendButton = document.querySelector('#sendBtn')
	if (sendButton) {
		sendButton.remove()
	}
	title.value = event.target.parentNode.childNodes[3].textContent.trim()
	body.value = event.target.parentNode.childNodes[5].textContent.trim()
	const editButton = document.querySelector('.edit-form-button')
	console.log(editButton)
	editButton.addEventListener('click', event => {
		console.log(editButton.getAttribute('data-my-card-id'))
		axios
			.put(
				`https://ajax.test-danit.com/api/json/posts/${editButton.getAttribute(
					'data-my-card-id'
				)}`,
				{
					title: title.value,
					body: body.value,
					userId: user.id,
				}
			)
			.then(({ status }) => {
				if (status === 200) {
					const cards = document.querySelectorAll('.post-card')
					cards[0].remove()
					cards[0].childNodes[3].childNodes[3] = title.value
					cards[0].childNodes[3].childNodes[5] = body.value
					console.log(cards[0].childNodes[3].childNodes[3])
					console.log(cards[0].childNodes[3].childNodes[5])
				}
			})
	})
}
const close = document.querySelector('.close')
const modal = document.querySelector('.modal')
const open = document.querySelector('#openModalBtn')
const addPost = document.querySelector('.add-post-form')
const title = document.querySelector('#title')
const body = document.querySelector('#body')
let postCount = 101
const modalFunction = (bool = false, event) => {
	const sendButton = document.querySelector('#sendBtn')
	if (sendButton) {
		sendButton.remove()
	}
	const editBtn = document.querySelector('.edit-form-button')
	if (editBtn) {
		editBtn.remove()
	}

	modal.style.display = 'block'
	const sendBtn = `<button id="sendBtn">Отправить</button>`
	const editButton = `<button class="edit-form-button" data-my-card-id="${event}">Редактировать</button>`
	addPost.insertAdjacentHTML('beforeend', bool ? editButton : sendBtn)
}

modal.addEventListener('click', e => {
	if (e.target === close || (e.target === modal && e.target !== addPost)) {
		modal.style.display = 'none'
		title.value = ''
		body.value = ''
		addPost.removeChild(addPost.lastChild)
	}
})
open.addEventListener('click', () => {
	modalFunction()
})
addPost.addEventListener('submit', e => {
	e.preventDefault()
	axios
		.post(`https://ajax.test-danit.com/api/json/posts`, {
			title: title.value,
			body: body.value,
			userId: user.id,
		})
		.then(({ status }) => {
			if (status === 200) {
				const newPost = new Card(
					user.id,
					title.value,
					body.value,
					0,
					'Just Now',
					true
				)
				const newPostHTML = newPost.createHTM(user, 99) // я хотел использовать postCount, но на сервере нет такого id поэтому приходится использовать 99 для моделирования ситуации
				container.insertAdjacentHTML('afterbegin', newPostHTML)
				modal.style.display = 'none'
				title.value = ''
				body.value = ''
				const lastLike = document.querySelector('.svg')
				lastLike.addEventListener('click', event => {
					likeFunction(event)
				})
				const deleteButton = document.querySelector('.delete-button')
				deleteButton.addEventListener('click', event => {
					deleteFunction(event)
				})
				editButton = document.querySelector('.edit-button')
				editButton.addEventListener('click', event => {
					editFunction(event)
				})
				postCount++
			}
		})
		.catch(error => {
			console.log(error)
		})
})
