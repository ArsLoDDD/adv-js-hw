class Employee {
	constructor(name, age, salary) {
		this._name = name
		this._age = age
		this._salary = salary
	}
	get name() {
		return this._name
	}
	get age() {
		return this._age
	}
	get salary() {
		return this._salary
	}
	set name(name) {
		this._name = name
	}
	set age(age) {
		this._age = age
	}
	set salary(salary) {
		this._salary = salary
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary)
		this._lang = lang
	}
	get salary() {
		return super.salary * 3
	}
	get lang() {
		return this._lang
	}
	set lang(lang) {
		this._lang = lang
	}
}

const programmer = new Programmer('John', 30, 2000, 'JavaScript')
const programmer2 = new Programmer('Jack', 28, 1800, 'Python')
const programmer3 = new Programmer('Mike', 25, 1600, 'Java')

console.log(programmer)
console.log(programmer2)
console.log(programmer3)
console.log(programmer.salary)

// /**
//  * @param {string[]} strs
//  * @return {string}
//  */
// var longestCommonPrefix = function (strs) {
// 	if (strs.length === 1) {
// 		return strs[0]
// 	}
// 	let newStrs = {}
// 	let emptyStr = false
// 	let bool = true
// 	for (let i = 0; i < strs.length; i++) {
// 		if (bool) {
// 			let element = strs[i].slice(0, 1)
// 			if (newStrs[element]) {
// 				newStrs[element]++
// 				if (newStrs[element] > 1) {
// 					emptyStr = true
// 				}
// 			} else {
// 				newStrs[element] = 1
// 			}
// 		} else {
// 			let elementScd = strs[i].slice(0, 2)
// 			if (newStrs[elementScd]) {
// 				newStrs[elementScd]++
// 			} else {
// 				newStrs[elementScd] = 1
// 			}
// 		}
// 		if (i === strs.length - 1 && bool) {
// 			bool = false
// 			i = -1
// 		}
// 	}
// 	if (emptyStr === false) {
// 		return ''
// 	}
// 	let maxValue = 1
// 	let maxKey = ''
// 	for (const str in newStrs) {
// 		console.log(str)
// 		console.log(str.length)
// 		console.log(maxKey.length)
// 		if (newStrs[str] >= maxValue && str.length > maxKey.length) {
// 			maxValue = newStrs[str]
// 			maxKey = str
// 		}
// 	}
// 	console.log(newStrs)
// 	return maxKey
// }
// console.log(longestCommonPrefix(['flower', 'flow', 'flight']))
