const btn = document.querySelector('#searchBtn')
const info = document.querySelector('.info')
let getIp
try {
	getIp = axios
		.get('https://api.ipify.org/?format=json')
		.then(res => (ip = res.data.ip))
} catch (error) {
	console.log(error)
}

const getInfoWithIp = async ip => {
	try {
		const { data } = await axios.get(`http://ip-api.com/json/${ip}`)
		const userInfo = ` <p><strong>IP-адрес:</strong> ${data.query}</p>
	<p><strong>Страна:</strong> ${data.country}</p>
	<p><strong>Код страны:</strong> ${data.countryCode}</p>
	<p><strong>Регион:</strong> ${data.region}</p>
	<p><strong>Название региона:</strong> ${data.regionName}</p>
	<p><strong>Город:</strong> ${data.city}</p>`
		info.innerHTML = userInfo
	} catch (error) {
		console.log(error)
	}
}

btn.addEventListener('click', async () => {
	let myIp = await getIp
	getInfoWithIp(myIp)
})
